image: Dockerfile
	docker build . -t pwilke/oscorrector
image-ro: image Dockerfile-ro
	docker build . -f Dockerfile-ro -t pwilke/oscorrector-ro
push: image
	docker push pwilke/oscorrector
push-ro: image-ro
	docker push pwilke/oscorrector-ro

release:
ifeq ($(VER),)
	echo "Gimme a version"
else
	echo ${VER} > VERSION
	git commit -am "Release ${VER}"
	git tag ${VER}
	git push --all
	git push --tags
endif
