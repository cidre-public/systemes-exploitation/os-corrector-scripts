FROM pwilke/os-infosec

RUN apt-get update --allow-releaseinfo-change
RUN apt-get install -y python3-pip clang libclang-dev
RUN pip3 install pexpect unidiff gitpython libclang==14.0.6 coloredlogs result requests packaging


RUN useradd -ms /bin/bash newuser
RUN mkdir /usr/local/corrector
RUN chown -R newuser /usr/local/corrector
RUN chmod a+w /data
USER newuser

ADD --chown=newuser:newuser testator /usr/local/corrector/testator
ADD --chown=newuser:newuser assertator.py /usr/local/corrector/
ADD --chown=newuser:newuser corrector.py /usr/local/corrector/
ADD --chown=newuser:newuser parsor.py /usr/local/corrector/
ADD --chown=newuser:newuser main.py /usr/local/corrector/
ADD --chown=newuser:newuser VERSION /usr/local/corrector/
WORKDIR /usr/local/corrector

ENV TZ='Europe/Paris'

ENTRYPOINT ["python3", "main.py"]
