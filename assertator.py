def test_not_none(v, descr):
    return (v is not None, descr, None)

def test_eq_int(lactual, lexpected, descr):
    if lactual == lexpected:
        return (True, descr, None)
    else:
        return (False, descr, "expected {}, got {}".format(lexpected, lactual))

def test_eq_list(lactual, lexpected, descr, sameorder=True):
    check = True
    if sameorder: check = lactual == lexpected
    else: check = sorted(lactual) == sorted(lexpected)
    if check:
        return (True, descr, None)
    else:
        return (False, descr, "expected {}, got {}".format(lexpected, lactual))

def test_dict(dactual, keys, fexpected, descr, sameorder=True):
    missing_keys = []
    superfluous_keys = []
    bad_mappings = []
    for k in keys:
        v = dactual.get(k)
        if v == None:
            missing_keys.append(k)
        else:
            if not (test_eq_list(v, fexpected(k), "", sameorder))[0]:
                bad_mappings.append(k)

    superfluous_keys = [k for k in dactual.keys() if k not in keys]
    if missing_keys == [] and superfluous_keys == [] and bad_mappings == []:
        return (True, descr, None)
    else:
        info = "\n"
        if missing_keys:
            info += "  Missing: {}\n".format(missing_keys)
        if superfluous_keys:
            info += "  Superfluous: {}\n".format(superfluous_keys)
        if bad_mappings:
            info += "  Bad mappings: "
            for k in bad_mappings:
                info += "    Key {}, expected {}, got {}\n".format(k, fexpected(k), dactual[k])
        return (False, descr, info)
