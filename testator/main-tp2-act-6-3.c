#include <stdio.h>
#include "stubs2.h"
#include "fat.h"

int main(){

  install_segv_handler();

  int i;
  struct proc* p;
  userinit();
  p = initproc;
  set_myproc(initproc);
  initproc->state = RUNNING;
  char *argv[] = {"_ls", "arg1", 0};
  int ret = exec("_ls", argv);
  char *str = easy_snprintf("exec(\"_ls\", {\"_ls\", \"arg1\", 0}) should return 2 (got %d)", ret);
  assert(ret == 2, str, NOSTOP);
  free(str);
  assert(myproc()->stack_vma != 0, "exec should have set stack_vma", NOSTOP);
  assert(myproc()->heap_vma != 0, "exec should have set heap_vma", NOSTOP);
  struct vma* ma = myproc()->memory_areas;
  int othervmaok = 1;
  while(ma){
    if (ma == myproc()->stack_vma){
      printf("stack vma [0x%lx,0x%lx] has permissions %c%c%c (expected RW-)\n",
             ma->va_begin, ma->va_end,
             (ma->vma_flags & VMA_R) ? 'R' : '-',
             (ma->vma_flags & VMA_W) ? 'W' : '-',
             (ma->vma_flags & VMA_X) ? 'X' : '-');
      assert(ma->vma_flags & VMA_R, "stack VMA should be readable", NOSTOP);
      assert(ma->vma_flags & VMA_W, "stack VMA should be writable", NOSTOP);
      assert(!(ma->vma_flags & VMA_X), "stack VMA should not be executable", NOSTOP);
    }
    else if (ma == myproc()->heap_vma){
      printf("heap vma [0x%lx,0x%lx] has permissions %c%c%c (expected RW-)\n",
             ma->va_begin, ma->va_end,
             (ma->vma_flags & VMA_R) ? 'R' : '-',
             (ma->vma_flags & VMA_W) ? 'W' : '-',
             (ma->vma_flags & VMA_X) ? 'X' : '-');
      assert(ma->vma_flags & VMA_R, "heap VMA should be readable", NOSTOP);
      assert(ma->vma_flags & VMA_W, "heap VMA should be writable", NOSTOP);
      assert(!(ma->vma_flags & VMA_X), "heap VMA should not be executable", NOSTOP);
    }
    else {
      printf("vma [0x%lx,0x%lx] has permissions %c%c%c (expected RWX)\n",
             ma->va_begin, ma->va_end,
             (ma->vma_flags & VMA_R) ? 'R' : '-',
             (ma->vma_flags & VMA_W) ? 'W' : '-',
             (ma->vma_flags & VMA_X) ? 'X' : '-');
      int ok = (ma->vma_flags & VMA_R) && (ma->vma_flags & VMA_W) && (ma->vma_flags & VMA_X);
      if(!ok) othervmaok = 0;
    }
    ma = ma->next;
  }
  assert(othervmaok, "Non-heap/stack VMAs should be RWX", NOSTOP);
  return 0;
}
