#include <stdio.h>
#include "stubs2.h"
#include "fat.h"

int main(){

  install_segv_handler();

  int i;
  struct proc* p;
  char* str;
  printf("Calling userinit\n");
  userinit();
  p = initproc;
  assert(p->state == RUNNABLE, "process created by userinit should be runnable", NOSTOP);
  struct vma* ma = p->memory_areas;
  int found = 0;
  i = 0;
  while(ma){
    if (i >= 2)
      assert(0, "would there be a loop in the process' VMAs?", STOP);
    if(ma->va_begin == 0 && ma->va_end == PGSIZE){
      found = 1;
    } else {
      char* fmt = "Found unexpected VMA [%lx, %lx[\n";
      int n = snprintf(NULL, 0, fmt, ma->va_begin, ma->va_end);
      char* str = malloc(n+1);
      snprintf(str, n+1, fmt, ma->va_begin, ma->va_end);
      assert(0, str, NOSTOP);
      free(str);
    }
    ma = ma->next;
    i++;
  }
  assert(found, "find expected VMA [0; PGSIZE]", NOSTOP);
  

  set_myproc(initproc);
  initproc->state = RUNNING;
  printf("Forking process obtained by userinit\n");
  int pid = fork();
  //printf("Fork returned %d\n", pid);
  str = easy_snprintf("fork should not return -1 (returned %d)", pid);
  assert(pid != -1, str, STOP);
  free(str);
  found = 0;
  struct proc* child = NULL;
  for(i = 0; i < NPROC; i++){
    if(proc[i].pid == pid){
      found = 1;
      child = &proc[i];
      break;
    }
  }
  assert(found, "find forked process!", STOP);
  assert(child->memory_areas != initproc->memory_areas, "VMAs of child and parent should be at different addresses", NOSTOP);

  ma = child->memory_areas;
  found = 0;
  i = 0;
  while(ma){
    if (i >= 2)
      assert(0, "forked: would there be a loop in the process' VMAs?", STOP);
    if(ma->va_begin == 0 && ma->va_end == PGSIZE){
      found = 1;
    } else {
      char* fmt = "forked: Found unexpected VMA [%lx, %lx[\n";
      int n = snprintf(NULL, 0, fmt, ma->va_begin, ma->va_end);
      char* str = malloc(n+1);
      snprintf(str, n+1, fmt, ma->va_begin, ma->va_end);
      assert(0, str, NOSTOP);
      free(str);
    }
    ma = ma->next;
    i++;
  }
  assert(found, "find expected VMA [0; PGSIZE]", NOSTOP);

  printf("From the child (pid=%d), calling exec on existing program _ls with args 'arg1'\n", pid);
  set_myproc(child);
  char *argv[] = {"_ls", "arg1", 0};
  int ret = exec("_ls", argv);
  str = easy_snprintf("exec(\"_ls\", {\"_ls\", \"arg1\", 0}) should return 2 (got %d)", ret);
  assert(ret == 2, str, NOSTOP);
  free(str);
  assert(myproc()->pid == child->pid, "exec should not have changed PID", NOSTOP);
  assert(myproc()->stack_vma != 0, "exec should have set stack_vma", NOSTOP);
  assert(myproc()->heap_vma != 0, "exec should have set heap_vma", NOSTOP);
  str = easy_snprintf("exec should have set heap_vma to an empty VMA (got [0x%lx; 0x%lx])",
                      myproc()->heap_vma ? myproc()->heap_vma->va_begin : -1,
                      myproc()->heap_vma ? myproc()->heap_vma->va_end : -1
                      );
  assert(myproc()->heap_vma && myproc()->heap_vma->va_begin == myproc()->heap_vma->va_end, str, NOSTOP);
  free(str);
  printf("myproc()->pid = %d\n", myproc()->pid);
  printf("myproc()->memory_areas = \n");
  ma = myproc()->memory_areas;
  while(ma){
    printf("  %lx - %lx\n", ma->va_begin, ma->va_end);
    ma = ma -> next;
  }

  printf("============\n");
  printf("Now, calling exec on NON-existing program _lls with args 'arg1'\n");
  struct vma* save_ma = myproc()->memory_areas;
  struct vma* save_stack_vma = myproc()->stack_vma;
  struct vma* save_heap_vma = myproc()->heap_vma;
  /* printf("save_ma: %lx\n", save_ma); */
  /* printf("save_stack_vma: %lx\n", save_stack_vma); */
  /* printf("save_heap_vma: %lx\n", save_heap_vma); */
  char *argv2[] = {"_lls", "arg1", 0};
  ret = exec("_lls", argv2);
  assert(ret == -1, "exec(\"_lls\", {\"_lls\", \"arg1\", 0}) should return -1", NOSTOP);
  assert(myproc()->stack_vma == save_stack_vma, "exec should not have modified stack_vma", NOSTOP);
  assert(myproc()->heap_vma == save_heap_vma, "exec should not have modified heap_vma", NOSTOP);
  assert(myproc()->memory_areas == save_ma, "exec should not have modified memory_ares", NOSTOP);
  /* printf("stack: %lx\n", myproc()->stack_vma); */
  /* printf("heap: %lx\n", myproc()->heap_vma); */
  /* printf("ma: %lx\n", myproc()->memory_areas); */


  return 0;
}
