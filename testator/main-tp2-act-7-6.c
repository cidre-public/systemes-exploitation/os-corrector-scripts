#include <stdio.h>
#include "stubs2.h"
#include "fat.h"

int main(){

  install_segv_handler();

  int i;
  struct proc* p;
  userinit();
  p = initproc;
  set_myproc(initproc);
  initproc->state = RUNNING;
  char *argv[] = {"_ls", "arg1", 0};
  int ret = exec("_ls", argv);
  char *str = easy_snprintf("exec(\"_ls\", {\"_ls\", \"arg1\", 0}) should return 2 (got %d)", ret);
  assert(ret == 2, str, NOSTOP);
  free(str);
  assert(myproc()->stack_vma != 0, "exec should have set stack_vma", NOSTOP);
  assert(myproc()->heap_vma != 0, "exec should have set heap_vma", NOSTOP);

  int vma_counter = 0;
  struct vma* ma = myproc()->memory_areas;
  struct { uint64 begin, end, flags, found; } expected[] =
    {
      { .begin = 0x0000, .end = 0x0000, .flags = 0, .found = 0 },
      { .begin = 0x0000, .end = 0x2000, .flags = VMA_R | VMA_X, .found = 0 },
      { .begin = 0x2000, .end = 0x3000, .flags = VMA_R, .found = 0 },
      { .begin = 0x3000, .end = 0x4000, .flags = VMA_R | VMA_W, .found = 0 },
    };
  while(ma){
    int found = 0;
    for(int i = 0; i < sizeof(expected)/sizeof(expected[0]); i++){
      if (ma->va_begin == expected[i].begin && ma->va_end == expected[i].end){
        expected[i].found = 1;
        found = 1;
        str = easy_snprintf("vma [%x,%x] should be %c%c%c (got %c%c%c)",
                            ma->va_begin, ma->va_end,
                            (expected[i].flags & VMA_R) != 0 ? 'R' : '-',
                            (expected[i].flags & VMA_W) != 0 ? 'W' : '-',
                            (expected[i].flags & VMA_X) != 0 ? 'X' : '-',
                            (ma->vma_flags & VMA_R) != 0 ? 'R' : '-',
                            (ma->vma_flags & VMA_W) != 0 ? 'W' : '-',
                            (ma->vma_flags & VMA_X) != 0 ? 'X' : '-'
                            );
        assert(expected[i].flags == (ma->vma_flags & expected[i].flags)
               , str, NOSTOP);
        free(str);
      }
    }
    if(!found && ma != myproc()->stack_vma && ma != myproc()->heap_vma) {
      str = easy_snprintf("unexpected vma [%x,%x] (file=%s, offset=0x%x, nbytes=0x%lx, perms=%c%c%c)",
                          ma->va_begin, ma->va_end,
                          ma->file, ma->file_offset, ma->file_nbytes,
                          (ma->vma_flags & VMA_R) != 0 ? 'R' : '-',
                          (ma->vma_flags & VMA_W) != 0 ? 'W' : '-',
                          (ma->vma_flags & VMA_X) != 0 ? 'X' : '-');
      assert(0, str, NOSTOP);
      free(str);
    }
    ma = ma->next;
  }
  for(int i = 0; i < sizeof(expected)/sizeof(expected[0]); i++){
    if (!expected[i].found){
      str = easy_snprintf("did not find vma [%x,%x] %c%c%c",
                          expected[i].begin, expected[i].end,
                          (expected[i].flags & VMA_R) != 0 ? 'R' : '-',
                          (expected[i].flags & VMA_W) != 0 ? 'W' : '-',
                          (expected[i].flags & VMA_X) != 0 ? 'X' : '-'
                          );
      assert(0, str, NOSTOP);
      free(str);
    }
  }


  return 0;
}
