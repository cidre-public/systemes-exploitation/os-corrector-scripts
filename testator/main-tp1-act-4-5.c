#include <stdio.h>
#include "stubs1.h"
#include "fat.h"




int main(){

  initator();

  proc[3].pid = 3;
  proc[4].pid = 4;

  set_filealloc_succeed(1);
  set_fdalloc_retval(3);

  printf("creating a mutex fd\n");
  int fd = sys_create_mutex();
  assert(fd >= 0, "sys_create_mutex should succeed", NOSTOP);
  assert(stub_files[fd] != 0, "mutex file should not be NULL", NOSTOP);
  if(stub_files[fd]){
    assert(stub_files[fd]->type == FD_MUTEX, "file type should be FD_MUTEX", NOSTOP);
    assert(stub_files[fd]->mutex.init, "mutex lock should be initialized", NOSTOP);
    assert(stub_files[fd]->mutex.locked == 0, "mutex lock should not be locked", NOSTOP);
  }

  printf("process pid=3 calls sys_release_mutex(fd)\n");
  set_myproc(&proc[3]);
  setup_syscall_args(1, fd);
  assert(sys_release_mutex() == -1, "release should fail (not acquired)", NOSTOP);
  printf("process pid=3 calls sys_acquire_mutex(fd)");
  setup_syscall_args(1, fd);
  assert(sys_acquire_mutex() == 0, "acquire should return 0", NOSTOP);
  printf("process pid=3 calls sys_release_mutex(fd)\n");
  setup_syscall_args(1, fd);
  assert(sys_release_mutex() == 0, "release should return 0", NOSTOP);
  printf("process pid=3 calls sys_acquire_mutex(fd)");
  setup_syscall_args(1, fd);
  assert(sys_acquire_mutex() == 0, "acquire should return 0", NOSTOP);
  set_myproc(&proc[4]);
  printf("process pid=4 calls sys_release_mutex(fd)\n");
  assert(sys_release_mutex() == -1, "release should fail", NOSTOP);
  set_myproc(&proc[3]);
  printf("process pid=3 calls sys_release_mutex(fd)\n");
  assert(sys_release_mutex() == 0, "release should succeed", NOSTOP);

  int badmutex = fd + 1;
  setup_syscall_args(1, badmutex);
  printf("process pid=3 calls sys_acquire_mutex(bad_file_descriptor)\n");
  assert(sys_acquire_mutex() == -1, "acquire should fail", NOSTOP);

  setup_syscall_args(1, fd);
  struct file* f = stub_files[fd];
  f->type = FD_DEVICE;
  printf("process pid=3 calls sys_acquire_mutex(non-mutex-fd)\n");
  assert(sys_acquire_mutex() == -1, "acquire should fail", NOSTOP);
  printf("process pid=3 calls sys_release_mutex(non-mutex-fd)\n");
  assert(sys_release_mutex() == -1, "release should fail", NOSTOP);

  return 0;
}
