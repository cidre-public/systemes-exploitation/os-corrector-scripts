#include <stdio.h>
#include "stubs2.h"
#include "fat.h"

pte_t my_pte = 0;

pte_t* walk(pagetable_t pt, uint64 va, int alloc){
  return &my_pte;
}

int main(){

  install_segv_handler();


  pagetable_t pt;
  int res;
  char* str;

  struct proc p;
  memset(&p, 0, sizeof(p));
  set_myproc(&p);

  struct vma* ma = add_memory_area(&p, 0x1000, 0x2000);
  ma->vma_flags = VMA_R;

  res = do_allocate(pt , myproc(), 0x1000, CAUSE_R);
  str = easy_snprintf("do_allocate CAUSE_R (VMA has VMA_R), should return 0 (got %d)", res);
  assert(res == 0, str, NOSTOP);
  free(str);

  str = easy_snprintf("PTE should now be UR--V (got V=%d, R=%d, W=%d, X=%d, U=%d)",
                      my_pte ? (my_pte & PTE_V) != 0 : 0,
                      my_pte ? (my_pte & PTE_R) != 0 : 0,
                      my_pte ? (my_pte & PTE_W) != 0 : 0,
                      my_pte ? (my_pte & PTE_X) != 0 : 0,
                      my_pte ? (my_pte & PTE_U) != 0 : 0
                      );
  assert((my_pte & (PTE_V|PTE_U|PTE_R)) == (PTE_V|PTE_U|PTE_R) &&
         (my_pte & (PTE_W|PTE_X)) == 0,
         str, NOSTOP);
  free(str);


  res = do_allocate(pt , myproc(), 0x1000, CAUSE_W);
  str = easy_snprintf("do_allocate CAUSE_W (VMA has VMA_R), should return EBADPERM (got %d)", res);
  assert(res == EBADPERM, str, NOSTOP);
  free(str);


  res = do_allocate(pt , myproc(), 0x1000, CAUSE_X);
  str = easy_snprintf("do_allocate CAUSE_X (VMA has VMA_R), should return EBADPERM (got %d)", res);
  assert(res == EBADPERM, str, NOSTOP);
  free(str);

  p.memory_areas = 0;
  my_pte = 0;

  /* On recommence ! */
  ma = add_memory_area(&p, 0x1000, 0x2000);
  ma->vma_flags = VMA_W;

  res = do_allocate(pt , myproc(), 0x1000, CAUSE_W);
  str = easy_snprintf("do_allocate CAUSE_W (VMA has VMA_W), should return 0 (got %d)", res);
  assert(res == 0, str, NOSTOP);
  free(str);

  str = easy_snprintf("PTE should now be U-W-V (got V=%d, R=%d, W=%d, X=%d, U=%d)",
                      my_pte ? (my_pte & PTE_V) != 0 : 0,
                      my_pte ? (my_pte & PTE_R) != 0 : 0,
                      my_pte ? (my_pte & PTE_W) != 0 : 0,
                      my_pte ? (my_pte & PTE_X) != 0 : 0,
                      my_pte ? (my_pte & PTE_U) != 0 : 0
                      );
  assert((my_pte & (PTE_V|PTE_U|PTE_W)) == (PTE_V|PTE_U|PTE_W) &&
         (my_pte & (PTE_R|PTE_X)) == 0,
         str, NOSTOP);
  free(str);


  res = do_allocate(pt , myproc(), 0x1000, CAUSE_R);
  str = easy_snprintf("do_allocate CAUSE_R (VMA has VMA_W), should return EBADPERM (got %d)", res);
  assert(res == EBADPERM, str, NOSTOP);
  free(str);


  res = do_allocate(pt , myproc(), 0x1000, CAUSE_X);
  str = easy_snprintf("do_allocate CAUSE_X (VMA has VMA_W), should return EBADPERM (got %d)", res);
  assert(res == EBADPERM, str, NOSTOP);
  free(str);

  p.memory_areas = 0;
  my_pte = 0;

  /* On recommence ! */
  ma = add_memory_area(&p, 0x1000, 0x2000);
  ma->vma_flags = VMA_X;

  res = do_allocate(pt , myproc(), 0x1000, CAUSE_X);
  str = easy_snprintf("do_allocate CAUSE_X (VMA has VMA_X), should return 0 (got %d)", res);
  assert(res == 0, str, NOSTOP);
  free(str);

  str = easy_snprintf("PTE should now be U--XV (got V=%d, R=%d, W=%d, X=%d, U=%d)",
                      my_pte ? (my_pte & PTE_V) != 0 : 0,
                      my_pte ? (my_pte & PTE_R) != 0 : 0,
                      my_pte ? (my_pte & PTE_W) != 0 : 0,
                      my_pte ? (my_pte & PTE_X) != 0 : 0,
                      my_pte ? (my_pte & PTE_U) != 0 : 0
                      );
  assert((my_pte & (PTE_V|PTE_U|PTE_X)) == (PTE_V|PTE_U|PTE_X) &&
         (my_pte & (PTE_R|PTE_W)) == 0,
         str, NOSTOP);
  free(str);


  res = do_allocate(pt , myproc(), 0x1000, CAUSE_R);
  str = easy_snprintf("do_allocate CAUSE_R (VMA has VMA_X), should return EBADPERM (got %d)", res);
  assert(res == EBADPERM, str, NOSTOP);
  free(str);


  res = do_allocate(pt , myproc(), 0x1000, CAUSE_W);
  str = easy_snprintf("do_allocate CAUSE_W (VMA has VMA_X), should return EBADPERM (got %d)", res);
  assert(res == EBADPERM, str, NOSTOP);
  free(str);



  return 0;
}
