#ifndef STUBS_H
#define STUBS_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>

extern jmp_buf segv_jmp_buf;
#define PGSIZE 4096 // bytes per page
#define PGSHIFT 12  // bits of offset within a page

#define NOFILE 24
#define HEAP_THRESHOLD (8*1024*1024)

// Asserts
#define USTACK_BOTTOM (256*1024*1024)
#define USTACK_LIMIT (4 * 1024)
#define USTACK_TOP (USTACK_BOTTOM + USTACK_LIMIT)

#define STOP (1)
#define NOSTOP (0)

#define VMA_R (1 << 1)
#define VMA_W (1 << 2)
#define VMA_X (1 << 3)

#define CAUSE_R 0xd
#define CAUSE_W 0xf
#define CAUSE_X 0xc



char* easy_snprintf(char* fmt, ...);
void assert(int condition, char* descr, int stop);

void install_segv_handler();




char* safestrcpy(char *s, const char *t, int n);
void panic(char* str);
extern int has_panicked;
typedef unsigned int  uint32;
typedef unsigned long uint64;
typedef uint64 *pagetable_t; // 512 PTEs
typedef uint64 pte_t;
typedef uint64 pde_t;
typedef unsigned char  uchar;


// shift a physical address to the right place for a PTE.
#define PA2PTE(pa) ((((uint64)pa) >> 12) << 10)

#define PTE2PA(pte) (((pte) >> 10) << 12)

#define PTE_FLAGS(pte) ((pte) & 0x3FF)

// extract the three 9-bit page table indices from a virtual address.
#define PXMASK          0x1FF // 9 bits
#define PXSHIFT(level)  (PGSHIFT+(9*(level)))
#define PX(level, va) ((((uint64) (va)) >> PXSHIFT(level)) & PXMASK)

#define MAXVA (1L << (9 + 9 + 9 + 12 - 1))

uint64 walkaddr(pagetable_t pagetable, uint64 va);

#define PTE_V (1L << 0) // valid
#define PTE_R (1L << 1)
#define PTE_W (1L << 2)
#define PTE_X (1L << 3)
#define PTE_U (1L << 4) // 1 -> user can access

#define ENOVMA     (-(1L << 1))
#define ENOMEM     (-(1L << 2))
#define ENOFILE    (-(1L << 3))
#define EMAPFAILED (-(1L << 4))
#define EBADPERM   (-(1L << 5))


#define PGROUNDUP(sz)  (((sz)+PGSIZE-1) & ~(PGSIZE-1))
#define PGROUNDDOWN(a) (((a)) & ~(PGSIZE-1))

#define PA2PTE(pa) ((((uint64)pa) >> 12) << 10)



void * bd_malloc(uint64 nbytes);
void bd_free(void *p);

void* kalloc();
void kfree(void*);
char* strdup(const char*);

void setup_syscall_args(int count, ...);

uint64 argraw(int n);

int argint(int n, int *ip);

int argaddr(int n, uint64 *ip);

int fetchaddr(uint64 addr, uint64* ip);


int fetchstr(uint64 addr, char *buf, int max);

int argstr(int n, char *buf, int max);

enum procstate { UNUSED, SLEEPING, RUNNABLE, RUNNING, ZOMBIE };

struct spinlock {
  int num;
  int date;
};

struct trapframe {
  uint64 epc, sp, a0, a1;
};


struct file {
  enum { FD_NONE, FD_PIPE, FD_INODE, FD_DEVICE } type;
  int ref; // reference count
  char readable;
  char writable;
  struct pipe *pipe; // FD_PIPE
  struct inode *ip;  // FD_INODE and FD_DEVICE
  uint off;          // FD_INODE and FD_DEVICE
  short major;       // FD_DEVICE
  short minor;       // FD_DEVICE
};



struct proc {
  struct spinlock lock;

  // p->lock must be held when using these:
  enum procstate state;        // Process state
  struct proc *parent;         // Parent process
  void *chan;                  // If non-zero, sleeping on chan
  int killed;                  // If non-zero, have been killed
  int xstate;                  // Exit status to be returned to parent's wait
  int pid;                     // Process ID

  int priority;                // Priority

  // these are private to the process, so p->lock need not be held.
  uint64 kstack;               // Virtual address of kernel stack
  uint64 sz;                   // Size of process memory (bytes)
  char name[16];               // Process name (debugging)
  char* cmd;
  struct vma* memory_areas;
  struct vma* heap_vma;
  struct vma* stack_vma;
  pagetable_t pagetable;
  struct trapframe* tf;
  struct inode* cwd;
  struct spinlock vma_lock;
  struct file * ofile[NOFILE];
};

struct list_proc {
  struct proc* p;
  struct list_proc* next;
};

struct inode* namei(char* path);

void acquire(struct spinlock* l);


void release(struct spinlock* l);

#define NPROC 64
#define NPRIO 10

extern struct proc proc[NPROC];

extern struct list_proc* prio[NPRIO];
extern struct spinlock prio_lock;


void insert_into_prio_queue(struct proc* p);
void remove_from_prio_queue(struct proc* p);

// Long-term locks for processes
struct sleeplock {
  uint locked;       // Is the lock held?
  struct spinlock lk; // spinlock protecting this sleep lock
  
  // For debugging:
  char *name;        // Name of lock.
  int pid;           // Process holding lock
  int init;
};

struct pipe {};
struct inode {};

extern struct file * stub_files[20];
void set_filealloc_succeed(int x);
void set_fdalloc_retval(int x);
struct file* filealloc(void);
int fdalloc(struct file*);
void fileclose(struct file* f);

extern struct file stub_file;
extern int stub_file_num;

void initsleeplock(struct sleeplock *lk, char *name);
int argfd(int n, int *pfd, struct file **pf);



void acquiresleep(struct sleeplock *lk);

void releasesleep(struct sleeplock *lk);

extern struct proc* _myproc;
void set_myproc(struct proc* p);
struct proc* myproc();

struct proc* allocproc();

extern struct proc* initproc;
void
uvminit(pagetable_t pagetable, uchar *src, uint sz);

struct vma {
  /* C'est une structure de liste chaînée : le pointeur next pointe sur la
   * prochaine VMA dans la liste. */
  struct vma * next;

  /* Le début et la fin de la VMA. */
  uint64 va_begin;
  uint64 va_end;

  /* Éventuellement, cette VMA peut être peuplée par le contenu d'un fichier. Si
   * c'est le cas, [file] donne le nom du fichier, [file_offset] le déplacement
   * dans le fichier, et [file_nbytes] le nombre d'octets à lire dans le
   * fichier.
   *
   * Par exemple, si [file_offset] vaut 0x300, [file_nbytes] vaut 0x60, [file]
   * vaut "toto", [va_begin] vaut 0x1000 et [va_end] vaut 0x1fff, alors on
   * chargera les octets 0x300-0x35f aux adresses virtuelles 0x1000-0x105f, et
   * les adresses virtuelles 0x1060-0x1fff seront remplies de 0.
   */
  char* file;
  uint64 file_offset;
  uint64 file_nbytes;

  /* Les permissions de cette VMA. (VMA_R, VMA_W, VMA_X) */
  unsigned char vma_flags;

  char* mem;
};

extern uchar initcode[50];
int
uvmcopy(pagetable_t old, pagetable_t new, uint64 sz);
void freeproc(struct proc *p);
struct file*    filedup(struct file*);
struct inode*   idup(struct inode*);

void end_op(int);
void begin_op(int);
void iput(struct inode* ip);
void            iunlockput(struct inode*);
void            iunlock(struct inode*);
void            ilock(struct inode*);
void            proc_freepagetable(pagetable_t, uint64);
char* strjoin(char **s);
size_t strlen(const char*);
int printf(const char* fmt, ...);
int
copyout(pagetable_t pagetable, uint64 dstva, char *src, uint64 len);
void
uvmclear(pagetable_t pagetable, uint64 va);
uint64
uvmalloc(pagetable_t pagetable, uint64 oldsz, uint64 newsz);
uint64
uvmdealloc(pagetable_t pagetable, uint64 oldsz, uint64 newsz);
struct inode*   namei(char*);
int             readi(struct inode*, int, uint64, uint, uint);
int
loadseg(pagetable_t pagetable, uint64 va, struct inode *ip, uint offset, uint sz);
void* memset(void*, int, size_t);
pagetable_t proc_pagetable(struct proc *p);

// Format of an ELF executable file

#define ELF_MAGIC 0x464C457FU  // "\x7FELF" in little endian

// File header
struct elfhdr {
  uint magic;  // must equal ELF_MAGIC
  uchar elf[12];
  ushort type;
  ushort machine;
  uint version;
  uint64 entry;
  uint64 phoff;
  uint64 shoff;
  uint flags;
  ushort ehsize;
  ushort phentsize;
  ushort phnum;
  ushort shentsize;
  ushort shnum;
  ushort shstrndx;
};

// Program section header
struct proghdr {
  uint32 type;
  uint32 flags;
  uint64 off;
  uint64 vaddr;
  uint64 paddr;
  uint64 filesz;
  uint64 memsz;
  uint64 align;
};

// Values for Proghdr type
#define ELF_PROG_LOAD           1

// Flag bits for Proghdr flags
#define ELF_PROG_FLAG_EXEC      1
#define ELF_PROG_FLAG_WRITE     2
#define ELF_PROG_FLAG_READ      4

#define ROOTDEV       0  // device number of file system root disk
#define MAXARG       32  // max exec arguments

struct vma *add_memory_area(struct proc *p, uint64 va_begin, uint64 va_end);
void free_vma(struct vma *vmas);
struct vma* get_memory_area(struct proc*, uint64);

char *get_memory_area_mem(struct proc *p, uint64 va);
extern void* memmove(void*, const void*, size_t);
#endif
