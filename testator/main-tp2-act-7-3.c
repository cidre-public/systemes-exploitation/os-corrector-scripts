#include <stdio.h>
#include "stubs2.h"
#include "fat.h"

pte_t my_pte[10];

pte_t* walk(pagetable_t pt, uint64 va, int alloc){
  int idx = va >> 12;
  if (idx < 10)
    return &my_pte[idx];
  return NULL;
}

uint64 expected_fso;
uint64 expected_nbytes;

int load_from_file(char* file, uint64 file_start_offset, uint64 pa, uint64 nbytes){
  char* str = easy_snprintf("load_from_file at file_start_offset = 0x%lx (expected 0x%lx), nbytes=0x%lx (expected 0x%lx)", file_start_offset, expected_fso, nbytes, expected_nbytes);
  assert(expected_fso == file_start_offset && expected_nbytes == nbytes,
         str, NOSTOP);
  free(str);
  printf("load_from_file %s, fso=%lx, nbytes=%lx\n", file, file_start_offset, nbytes);
  return 0;
}

int main(){

  install_segv_handler();

  pagetable_t pt;
  int res;
  char* str;

  struct proc p;
  memset(&p, 0, sizeof(p));
  set_myproc(&p);
  struct vma* ma = add_memory_area(&p, 0, 0x4000);
  ma->vma_flags = VMA_R;
  ma->file = "vma-file";
  ma->file_offset = 0x100;
  ma->file_nbytes = 0x3300;
  printf("we set up a VMA ma: [0; 0x4000] with permissions R-- with\n");
  printf("  ma->file = '%s' \n", ma->file);
  printf("  ma->file_offset = 0x%lx\n", ma->file_offset);
  printf("  ma->file_nbytes = 0x%lx\n", ma->file_nbytes);
  printf("do_allocate with va = 0\n");
  expected_fso = 0x100;
  expected_nbytes = 0x1000;
  acquire(&p.vma_lock);
  res = do_allocate(pt, myproc(), 0, CAUSE_R);
  release(&p.vma_lock);
  str = easy_snprintf("do_allocate should succeed (got %d)", res);
  assert (res == 0, str, NOSTOP);
  free(str);

  printf("do_allocate with va = 0x2000\n");
  expected_fso = 0x2100;
  expected_nbytes = 0x1000;
  acquire(&p.vma_lock);
  res = do_allocate(pt, myproc(), 0x2500, CAUSE_R);
  release(&p.vma_lock);
  str = easy_snprintf("do_allocate should succeed (got %d)", res);
  assert (res == 0, str, NOSTOP);
  free(str);

  printf("do_allocate with va = 0x3f00\n");
  expected_fso = 0x3100;
  expected_nbytes = 0x300;
  acquire(&p.vma_lock);
  res = do_allocate(pt, myproc(), 0x3f00, CAUSE_R);
  release(&p.vma_lock);
  str = easy_snprintf("do_allocate should succeed (got %d)", res);
  assert (res == 0, str, NOSTOP);
  free(str);


  return 0;
}
