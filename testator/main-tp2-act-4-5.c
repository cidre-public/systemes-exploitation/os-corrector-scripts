#include <stdio.h>
#include "stubs2.h"
#include "fat.h"

pte_t* walk(pagetable_t pt, uint64 va, int alloc){
  pte_t* pte = calloc(1, sizeof(pte_t));
  switch(va){
  case 0:
    return 0;
  case 0x1000:
    break;
  case 0x2000:
    *pte |= PTE_V;
    break;
  case 0x3000:
    *pte |= PTE_U;
    break;
  case 0x4000:
    *pte |= PTE_V;
    *pte |= PTE_U;
    break;
  }
  return pte;
}

int main(){

  install_segv_handler();

  /* printf("In these tests, walk has the following behavior:\n"); */
  /* printf("virtual address 0x0000 -> NULL\n"); */
  /* printf("virtual address 0x1000 -> pte such that *pte = 0\n"); */
  /* printf("virtual address 0x2000 -> pte such that *pte | PTE_V = 1\n"); */
  /* printf("virtual address 0x3000 -> pte such that *pte | PTE_U = 1 (but *pte | PTE_V = 0)\n"); */
  /* printf("virtual address 0x4000 -> pte such that *pte | PTE_V = 1 and *pte | PTE_U = 1\n"); */

  pagetable_t pt;
  int res;

  printf("Calling do_allocate with walk(addr) = 0\n");
  if(setjmp(segv_jmp_buf)){
    goto l1;
  }
  res = do_allocate(pt, myproc(), 0);
  assert(res == ENOMEM, "do_allocate should return ENOMEM on null PTE", NOSTOP);
 l1:
  if(setjmp(segv_jmp_buf)){
    goto l2;
  }
  printf("Calling do_allocate with walk(addr) = non null PTE (V=0, U=0)\n");
  res = do_allocate(pt, myproc(), 0x1000);
  assert(res == ENOMEM, "do_allocate should return ENOMEM on PTE (V=0, U=0)", NOSTOP);
 l2:
  if(setjmp(segv_jmp_buf)){
    goto l3;
  }
  printf("Calling do_allocate with walk(addr) = non null PTE (V=1, U=0)\n");
  res = do_allocate(pt, myproc(), 0x2000);
  assert(res == EBADPERM, "do_allocate should return EBADPERM on PTE (V=1, U=0)", NOSTOP);
 l3:
  if(setjmp(segv_jmp_buf)){
    goto l4;
  }
  printf("Calling do_allocate with walk(addr) = non null PTE (V=0, U=1)\n");
  res = do_allocate(pt, myproc(), 0x3000);
  assert(res == ENOMEM, "do_allocate should return ENOMEM on PTE (V=0, U=1)", NOSTOP);
 l4:
  if(setjmp(segv_jmp_buf)){
    goto l5;
  }
  printf("Calling do_allocate with walk(addr) = non null PTE (V=1, U=1)\n");
  res = do_allocate(pt, myproc(), 0x4000);
  assert(res == 0, "do_allocate should return 0 on PTE (V=1, U=1)", NOSTOP);
 l5:

  return 0;
}
