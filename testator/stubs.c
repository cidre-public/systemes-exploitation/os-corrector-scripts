#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "stubs.h"

struct file stub_file;
int stub_file_num;
struct file * stub_files[20];
struct proc* _myproc;

void assert(int condition, char* descr, int stop){
  if(!condition){
    printf("KO %s\n", descr);
    if(stop) exit(EXIT_FAILURE);
  }
  else {
    printf("OK %s\n", descr);
  }
}

static void handler(int sig, siginfo_t *si, void *unused)
{
  printf("Got SIGSEGV at address: 0x%lx\n",(long) si->si_addr);
  printf("Got SIGSEGV at range 0x%lx--0x%lx\n",(long) si->si_lower, (long)si->si_upper);
  exit(EXIT_FAILURE);
}

void install_segv_handler(){
  struct sigaction sa;

  sa.sa_flags = SA_SIGINFO;
  sigemptyset(&sa.sa_mask);
  sa.sa_sigaction = handler;
  if (sigaction(SIGSEGV, &sa, NULL) == -1){
    perror("sigaction");
    exit(EXIT_FAILURE);
  }
}

static uint64 syscall_args[6];
void setup_syscall_args(int count, ...){
  va_list args;
  va_start(args, count);
  if (count > 6) count = 6;
  for(int i = 0; i < count; i++){
    syscall_args[i] = va_arg(args, uint64);
  }
  va_end(args);
}

uint64 argraw(int n){
  if (0 <= n && n < 6) return syscall_args[n];
  return -1;
}

int argint(int n, int *ip){
  *ip = argraw(n);
  return 0;
}

int argaddr(int n, uint64 *ip){
  *ip = argraw(n);
  return 0;
}

int
fetchaddr(uint64 addr, uint64* ip)
{
  uint64* p = (uint64*)addr;
  *ip = *p;
  return 0;
}


int
fetchstr(uint64 addr, char *buf, int max)
{
  strncpy(buf, (char*)addr, max);
  return strlen(buf);
}

int
argstr(int n, char *buf, int max)
{
  uint64 addr;
  if(argaddr(n, &addr) < 0)
    return -1;
  return fetchstr(addr, buf, max);
}

void acquire(struct spinlock* l){
  l->num++;
}

void release(struct spinlock* l){
  l->num--;
}

int holding(struct spinlock *lk)
{
  return lk->num != 0;
}

struct proc proc[NPROC];
struct list_proc* prio[NPRIO];
struct spinlock prio_lock;
int called = 0;

// Needs lock on p and prio_lock[p->priority]
void insert_into_prio_queue(struct proc* p){
  called = 1;
  if (!holding(&p->lock)){
    assert(0, "insert_into_prio_queue: should hold p->lock", 1);
  }
  if (!holding(&prio_lock)){
    assert(0, "insert_into_prio_queue: should hold prio_lock", 1);
  }
  struct list_proc* new = bd_malloc(sizeof(struct list_proc));
  new->next = 0;
  new->p = p;
  if(!prio[p->priority]){
    prio[p->priority] = new;
  }
  else {
    struct list_proc* last = prio[p->priority];
    while(last && last->next){
      last = last->next;
    }
    last->next = new;
  }
}

// Needs lock on p and prio_lock[p->priority]
void remove_from_prio_queue(struct proc* p){
  called = 1;
  if (!holding(&p->lock)){
    assert(0, "remove_from_prio_queue: should hold p->lock", 1);
  }
  if (!holding(&prio_lock)){
    assert(0, "remove_from_prio_queue: should hold prio_lock", 1);
  }
  struct list_proc* old = prio[p->priority];
  struct list_proc* prev = 0;
  struct list_proc* head = old;

  while(old){
    if(old->p == p) {
      if(old == head){
        head = old->next;
      } else {
        prev->next = old->next;
      }
      bd_free(old);
      break;
    }
    prev = old;
    old = old->next;
  }

  prio[p->priority] = head;
}

static int filealloc_succeed = 0;
void set_filealloc_succeed(int x){
  filealloc_succeed = x;
}

static int fdalloc_retval = 0;
void set_fdalloc_retval(int x){
  fdalloc_retval = x;
}

/* struct file stub_file = */
/*   { */
    
/*   }; */

struct file*
filealloc(void)
{
  if(filealloc_succeed){
    stub_file_num++;
    return &stub_file;
  } else {
    return 0;
  }
}



int argfd(int n, int *pfd, struct file **pf){
  int fd;
  struct file * f;
  if(argint(n, &fd) < 0) return -1;
  if(fd < 0 || fd >= 20 || (f=stub_files[fd]) == 0)
    return -1;
  if(pfd) *pfd = fd;
  if(pf) *pf = f;
  return 0;
}

int fdalloc(struct file *f){
  if(fdalloc_retval >= 0 && fdalloc_retval < 20){
    stub_files[fdalloc_retval] = f;
  }
  return fdalloc_retval;
}

void fileclose(struct file* f){
  assert(f == &stub_file, "fileclose", NOSTOP);
  stub_file_num--;
}

void
initsleeplock(struct sleeplock *lk, char *name){
  lk->init = 1;
}

void set_myproc(struct proc* p){ _myproc = p; }

struct proc* myproc(){
  return _myproc;
}

void
acquiresleep(struct sleeplock *lk)
{
  acquire(&lk->lk);
  lk->locked = 1;
  lk->pid = myproc()->pid;
  release(&lk->lk);
}

void
releasesleep(struct sleeplock *lk)
{
  acquire(&lk->lk);
  lk->locked = 0;
  lk->pid = 0;
  release(&lk->lk);
}

void * bd_malloc(uint64 nbytes){
  return malloc(nbytes);
}
void bd_free(void *p){
  free(p);
}
