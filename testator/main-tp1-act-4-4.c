#include <stdio.h>
#include "stubs1.h"
#include "fat.h"

int main(){

  initator();

  set_filealloc_succeed(0);
  set_fdalloc_retval(3);
  assert(sys_create_mutex() == -1, "filealloc fails: sys_create_mutex should return -1", NOSTOP);

  set_filealloc_succeed(1);
  set_fdalloc_retval(-1);
  assert(sys_create_mutex() == -1, "filealloc succeeds, fdalloc fails: sys_create_mutex should return -1", NOSTOP);

  set_filealloc_succeed(1);
  set_fdalloc_retval(3);

  int fd = sys_create_mutex();
  assert(fd >= 0, "filealloc succeeds, fdalloc succeeds: sys_create_mutex should return a non negative value", NOSTOP);
  assert(stub_files[fd] != 0, "file returned not NULL", NOSTOP);
  if(stub_files[fd]){
    assert(stub_files[fd]->type == FD_MUTEX, "file type should be FD_MUTEX", NOSTOP);
    assert(stub_files[fd]->mutex.init, "mutex lock should be initialized", NOSTOP);
    assert(stub_files[fd]->mutex.locked == 0, "mutex lock should not be locked", NOSTOP);
  }
  return 0;
}
