#include <stdio.h>
#include "stubs1.h"
#include "fat.h"

void print_prio_queue(struct list_proc* l){
  printf("[");
  while(l){
    if(l->p)
      printf("%d", l->p->pid);
    else
      printf("(null)");
    if (l->next)
      printf(", ");
    l = l->next;
  }
  printf("]");
}

int main(){
  initator();

  int i;
  struct proc* p;
  for(i = 0; i < NPROC; i++){
    proc[i].state = UNUSED;
  }


  printf("Calling userinit...\n");
  userinit();

  assert (prio[DEF_PRIO] != NULL, "default priority queue should not be empty", STOP);

  printf("Priority queue of default priority is: ");
  print_prio_queue(prio[DEF_PRIO]);
  printf("\n");
  
  assert (prio[DEF_PRIO]->p == initproc, "first process of default priority queue is initproc", NOSTOP);
  
  set_myproc(initproc);

  printf("Forking initproc...\n");
  fork();

  printf("Priority queue of default priority is: ");
  print_prio_queue(prio[DEF_PRIO]);
  printf("\n");

  assert (prio[DEF_PRIO]->p == initproc, "first process of default priority queue is initproc", NOSTOP);

  assert (prio[DEF_PRIO]->next != NULL, "default priority queue should have 2 elements", STOP);

  assert (prio[DEF_PRIO]->next->p == &proc[1], "second process of default priority queue is &proc[1]", NOSTOP);

  printf("initproc now gets priority 3, forking again...\n");

  initproc->priority = 3;

  fork();

  printf("After fork:\n");
  printf("Priority queue of default priority is: ");
  print_prio_queue(prio[DEF_PRIO]);
  printf("\n");
  printf("Priority queue of priority 3 is: ");
  print_prio_queue(prio[3]);
  printf("\n");

  assert (prio[DEF_PRIO]->p == initproc, "first process of default priority queue is initproc", NOSTOP);

  assert (prio[DEF_PRIO]->next->p == &proc[1], "second process of default priority queue is &proc[1]", NOSTOP);

 assert (prio[DEF_PRIO]->next->next == NULL, "default priority queue should still have 2 elements", NOSTOP);

 assert (prio[3] != NULL, "priority queue no 3 should not be empty", NOSTOP);
 if (prio[3] != NULL){
   assert (prio[3]->p == &proc[2], "priority queue no 3 should hold &proc[2]", NOSTOP);

   assert (prio[3]->next == NULL, "priority queue no 3 should have only 1 process", NOSTOP);
 }

 
 if(setjmp(sched_jmp_buf)){
   goto lab1;
 }

 set_myproc(&proc[2]);
 printf("Exiting from proc[2] with status 42\n");
 exit(42);
 assert(0, "exit should not return!", STOP);
 lab1:
 // back into the scheduler
 // releasing lock on proc[2], taken by exit
 release (&proc[2].lock);

 printf("proc[2]: state = %s, xstate = %d\n", proc_state_to_string(proc[2].state), proc[2].xstate);

 assert (proc[2].state == ZOMBIE, "proc[2] should be ZOMBIE", NOSTOP);
 assert (proc[2].xstate == 42, "proc[2] should have an exit status of 42", NOSTOP);

 set_myproc(initproc);

 printf("Waiting from initproc\n");
 wait(0);

 printf("Priority queue of priority 3 is: ");
 print_prio_queue(prio[3]);
 printf("\n");

 assert (prio[3] == NULL, "priority queue no 3 should be empty", NOSTOP);

 printf("Killing proc[1]\n");
 kill(proc[1].pid);
 printf("proc[1]: state = %s, xstate = %d, killed = %d\n",
        proc_state_to_string(proc[1].state),
        proc[1].xstate,
        proc[1].killed);

 assert(proc[1].killed == 1, "proc[1] should be killed.", NOSTOP);
 
 set_myproc(&proc[1]);
 
 if(setjmp(sched_jmp_buf)){
   goto lab2;
 }
 printf("Switching to proc[1], which calls exit(-1)\n");
 exit(-1);
 assert(0, "exit should not return!", STOP);
 lab2:
 release (&proc[1].lock);

 printf("proc[1]: state = %s, xstate = %d, killed = %d\n",
        proc_state_to_string(proc[1].state),
        proc[1].xstate,
        proc[1].killed);

 assert (proc[1].state == ZOMBIE, "proc[1] should be ZOMBIE", NOSTOP);
 assert (proc[1].xstate == -1, "proc[1] should have an exit status of -1", NOSTOP);

 set_myproc(initproc);

 printf("Waiting from initproc\n");
 wait(0);

 printf("Priority queue of priority 5 is: ");
 print_prio_queue(prio[5]);
 printf("\n");

 assert (prio[5] != NULL, "priority queue no 5 should not be empty", NOSTOP);
 assert (prio[5]->p == initproc, "priority queue no 5 should contain initproc", NOSTOP);
 assert (prio[5]->next == NULL, "priority queue no 5 should contain only 1 process", NOSTOP);


 original_exit(12);


  return 0;
}
