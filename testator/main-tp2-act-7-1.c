#include <stdio.h>
#include "stubs2.h"
#include "fat.h"

int main(){

  install_segv_handler();

  int i;
  struct proc* p;
  userinit();
  p = initproc;
  set_myproc(initproc);
  initproc->state = RUNNING;
  char *argv[] = {"_ls", "arg1", 0};
  int ret = exec("_ls", argv);
  char *str = easy_snprintf("exec(\"_ls\", {\"_ls\", \"arg1\", 0}) should return 2 (got %d)", ret);
  assert(ret == 2, str, NOSTOP);
  free(str);
  assert(myproc()->stack_vma != 0, "exec should have set stack_vma", NOSTOP);
  assert(myproc()->heap_vma != 0, "exec should have set heap_vma", NOSTOP);

  int vma_counter = 0;
  struct vma* ma = myproc()->memory_areas;
  while(ma){
    if (ma->va_begin == 0 && ma->va_end == 0){
      vma_counter++;
      str = easy_snprintf("vma [%x,%x] should be associated with file '_ls' (got '%s') with offset=0x1c02 (got 0x%lx) and nbytes=0 (got 0x%lx)",
                          ma->va_begin, ma->va_end,
                          ma->file, ma->file_offset, ma->file_nbytes);
      assert(ma->file != 0 &&
             !strcmp(ma->file, "_ls") &&
             ma->file_offset == 0x1c02 &&
             ma->file_nbytes == 0
             , str, NOSTOP);
      free(str);
    } else if (ma->va_begin == 0 && ma->va_end == 0x2000){
      vma_counter++;
      str = easy_snprintf("vma [%x,%x] should be associated with file '_ls' (got '%s') with offset=0x0 (got 0x%lx) and nbytes=0x1c02 (got 0x%lx)",
                          ma->va_begin, ma->va_end,
                          ma->file, ma->file_offset, ma->file_nbytes);
      assert(ma->file != 0 &&
             !strcmp(ma->file, "_ls") &&
             ma->file_offset == 0 &&
             ma->file_nbytes == 0x1c02
             , str, NOSTOP);
      free(str);
    } else if (ma->va_begin == 0x2000 && ma->va_end == 0x3000){
      vma_counter++;
      str = easy_snprintf("vma [%x,%x] should be associated with file '_ls' (got '%s') with offset=0x2000 (got 0x%lx) and nbytes=0x90 (got 0x%lx)",
                          ma->va_begin, ma->va_end,
                          ma->file, ma->file_offset, ma->file_nbytes);
      assert(ma->file != 0 &&
             !strcmp(ma->file, "_ls") &&
             ma->file_offset == 0x2000 &&
             ma->file_nbytes == 0x90
             , str, NOSTOP);
      free(str);
    } else if (ma->va_begin == 0x3000 && ma->va_end == 0x4000){
      vma_counter++;
      str = easy_snprintf("vma [%x,%x] should be associated with file '_ls' (got '%s') with offset=0x3000 (got 0x%lx) and nbytes=0 (got 0x%lx)",
                          ma->va_begin, ma->va_end,
                          ma->file, ma->file_offset, ma->file_nbytes);
      assert(ma->file != 0 &&
             !strcmp(ma->file, "_ls") &&
             ma->file_offset == 0x3000 &&
             ma->file_nbytes == 0
             , str, NOSTOP);
      free(str);
    } else if (ma == myproc()->stack_vma){
      vma_counter++;
    }
    else if (ma == myproc()->heap_vma){
      vma_counter++;
    }
    else {
      str = easy_snprintf("unexpected vma [%x,%x] (file=%s, offset=0x%x, nbytes=0x%lx)",
                          ma->va_begin, ma->va_end,
                          ma->file, ma->file_offset, ma->file_nbytes);
      assert(0, str, NOSTOP);
      free(str);
    }
    ma = ma->next;

  }
  str = easy_snprintf("there should be 6 vmas after exec(_ls) (got %d)", vma_counter);
  assert (vma_counter == 6, str, NOSTOP);
  free(str);


  return 0;
}
