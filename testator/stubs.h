#ifndef STUBS_H
#define STUBS_H

#include <stdlib.h>

// Asserts


#define STOP (1)
#define NOSTOP (0)

void assert(int condition, char* descr, int stop);

void install_segv_handler();





typedef unsigned long uint64;

void * bd_malloc(uint64 nbytes);
void bd_free(void *p);

void setup_syscall_args(int count, ...);

uint64 argraw(int n);

int argint(int n, int *ip);

int argaddr(int n, uint64 *ip);

int fetchaddr(uint64 addr, uint64* ip);


int fetchstr(uint64 addr, char *buf, int max);

int argstr(int n, char *buf, int max);

enum procstate { UNUSED, SLEEPING, RUNNABLE, RUNNING, ZOMBIE };

struct spinlock {
  int num;
};

struct proc {
  struct spinlock lock;

  // p->lock must be held when using these:
  enum procstate state;        // Process state
  struct proc *parent;         // Parent process
  void *chan;                  // If non-zero, sleeping on chan
  int killed;                  // If non-zero, have been killed
  int xstate;                  // Exit status to be returned to parent's wait
  int pid;                     // Process ID

  int priority;                // Priority

  // these are private to the process, so p->lock need not be held.
  uint64 kstack;               // Virtual address of kernel stack
  uint64 sz;                   // Size of process memory (bytes)
  char name[16];               // Process name (debugging)
  char* cmd;
};

struct list_proc {
  struct proc* p;
  struct list_proc* next;
};

void acquire(struct spinlock* l);


void release(struct spinlock* l);

#define NPROC 64
#define NPRIO 10

extern struct proc proc[NPROC];

extern struct list_proc* prio[NPRIO];
extern struct spinlock prio_lock;


void insert_into_prio_queue(struct proc* p);
void remove_from_prio_queue(struct proc* p);

// Long-term locks for processes
struct sleeplock {
  uint locked;       // Is the lock held?
  struct spinlock lk; // spinlock protecting this sleep lock
  
  // For debugging:
  char *name;        // Name of lock.
  int pid;           // Process holding lock
  int init;
};

struct pipe {};
struct inode {};

struct file {
  enum { FD_NONE, FD_PIPE, FD_INODE, FD_DEVICE, FD_MUTEX } type;
  int ref; // reference count
  char readable;
  char writable;
  struct pipe *pipe; // FD_PIPE
  struct inode *ip;  // FD_INODE and FD_DEVICE
  uint off;          // FD_INODE and FD_DEVICE
  short major;       // FD_DEVICE
  short minor;       // FD_DEVICE
  struct sleeplock mutex;
};

extern struct file * stub_files[20];
void set_filealloc_succeed(int x);
void set_fdalloc_retval(int x);
struct file* filealloc(void);
int fdalloc(struct file*);
void fileclose(struct file* f);

extern struct file stub_file;
extern int stub_file_num;

void initsleeplock(struct sleeplock *lk, char *name);
int argfd(int n, int *pfd, struct file **pf);



void acquiresleep(struct sleeplock *lk);

void releasesleep(struct sleeplock *lk);

extern struct proc* _myproc;
void set_myproc(struct proc* p);
struct proc* myproc();

extern int called;

#endif
